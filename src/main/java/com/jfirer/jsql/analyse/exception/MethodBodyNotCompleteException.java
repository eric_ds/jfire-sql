package com.jfirer.jsql.analyse.exception;

public class MethodBodyNotCompleteException extends RuntimeException
{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public MethodBodyNotCompleteException()
    {
    }
}
