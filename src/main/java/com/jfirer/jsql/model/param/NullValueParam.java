package com.jfirer.jsql.model.param;

import com.jfirer.jsql.model.support.SFunction;

public class NullValueParam extends InternalParamImpl
{
    public NullValueParam(SFunction<?, ?> fn)
    {
        super(fn);
        consumer = (columnName, builder, paramValues) -> {
            builder.append(columnName).append(" is null ");
        };
    }
}
