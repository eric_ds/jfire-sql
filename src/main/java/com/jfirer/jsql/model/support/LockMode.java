package com.jfirer.jsql.model.support;

public enum LockMode
{
    SHARE,
    UPDATE
}
