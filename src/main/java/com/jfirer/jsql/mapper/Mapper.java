package com.jfirer.jsql.mapper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标记当前的接口是一个Mapper接口，Jsql会根据注解或者方法名为其提供实现
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Mapper
{
    /**
     * 本 Mapper 中会使用的代表实体的类
     */
    Class<?>[] value() default {};
}
